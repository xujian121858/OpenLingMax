/*
 * @Date: 2020-02-06 03:48:31
 * @Author: 	LingMax[xiaohxx@qq.com]
 * @最后编辑: 	LingMax[xiaohxx@qq.com]
 * @文件详情: 	文件详情
 */

var 
fs = require("fs"),
path = require('path'),
cwd = process.cwd(),
child_process = require('child_process'),
iconv =  require('iconv-lite'),//iconv.encode(text, 'gbk'); iconv.decode(buffer,'GBK');
cryptoX = require('crypto'),
request = require('request'),
stream = require('stream'),
zlib = require('zlib');
// /var win = nw.Window.get(); //win 窗口对象
//console.log(nw);
var option = {
    key : "Alt+Z",
    active : function() {
        global.maxxssy();
        //console.log("全局快捷键: " + this.key + " 被激活."); 
    },
    failed : function(msg) {
        // :(, 无法注册 |key| 或未注册 |key|.
        console.log(msg);
        throw msg;
    }
};

// 使用 |option| 注册快捷键
var shortcut = new nw.Shortcut(option);

// 注册全快捷键 即使无聚焦也可工作
nw.App.registerGlobalHotKey(shortcut);

require(process.cwd()+"/js/locadCache.js");