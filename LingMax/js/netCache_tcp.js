/*
 * @Date: 2020-01-07 21:44:18
 * @Author: 	LingMax[xiaohxx@qq.com]
 * @最后编辑: 	LingMax[xiaohxx@qq.com]
 * @文件详情: 	网络优化模块  请尊重原著 借鉴本功能 请备注出处 [开发者神器库 by:LingMax]
 */
//var arr = [];for (const key in temp1) {arr.push(temp1[key].name) ;}

var 
dgram      = require('dgram'),
udpServer  = dgram.createSocket('udp4'),//监听53端口
dp         = require('dns-packet'),
Dcache     = {},//dns缓存
Ecache     = {},//已经优化过的 缓存
ping       = require ("net-ping"),
net        = require('net'),
dns        = require('dns'),
//fs         = require('fs'),
arrLd      = [],//列队 限速,限制不卡
adasdadsad;
var ddaatt = [];//缓存
//const ipS = new (require('maxmind')).Reader(fs.readFileSync(__dirname + '/GeoLite2-City.mmdb'));
//console.log(lookup.get('66.6.44.4'));
var ztzt =  true;//是否打开DNS加速
var delCacheTime = 40*60*1000;//40分钟
var DNS_jhc = true;//走旧缓存 昨天历史数据

var lastDcache;// 昨天历史数据
(function () {
   var e;
   var dd = {'dd':new Date().getDate()*100+new Date().getMonth()+1,'jc':999999,'ip':'','tt':new Date().getTime()};
try {
   lastDcache = JSON.parse(fs.readFileSync('data/DNSlistCache.json').toString());
   //缓存是否超过10天
   if(!lastDcache["time_lingMax_hahaha"] || dd.tt-lastDcache["time_lingMax_hahaha"].tt>864000000/*1000*10*24*60*60*/ )lastDcache = {"time_lingMax_hahaha":dd};
   if (lastDcache["time_lingMax_hahaha"].dd==dd.dd) return;
   for (const key in lastDcache) {
       e = lastDcache[key];
       e.jc=parseInt(e.jc*0.8);//砍掉2成
   }
   lastDcache["time_lingMax_hahaha"]=dd
} catch (error) {
   lastDcache = {"time_lingMax_hahaha":dd};
}
})();

/**
* 保存 昨天历史数据
*/
function ssLastDcache(){
   var e,arr=[],ls={};

   //解析
   for (const key in lastDcache) {
       e = lastDcache[key];
       e.host=key;
       arr.push(e);
   }

   //排序 倒序 并且获取前1500个
   arr = arr.sort((a,b)=>{
       return b.jc-a.jc
   }).slice(0,1500);
   console.log();
   
   //还原
   for (const key in arr) {
       e=arr[key];
       ls[e.host] = e;
       delete e.host;
   }
   //保存
   fs.writeFileSync('data/DNSlistCache.json', JSON.stringify(ls));
}



setInterval(() => {
   var tt = new Date().getTime();
   for (const key in Dcache) {
       if(Dcache[key].ltime <= tt){
           //console.log(Dcache[key]);
           
           delete Ecache[Dcache[key].host];
           delete ddaatt[Dcache[key].host];
           delete Dcache[key];
       }
           
   }
   for (const key in sendList) {
       if(sendList[key].time <= tt){
           delete sendList[key];//删除查询缓存
       }
   }

}, 60*1000+50);;//一分钟 删除一次过期的

//开始  解析114 事件处理
var udpC = dgram.createSocket('udp4');
//setTimeout(()=>{udpC.close();},2000);//定时销毁
udpC.on('message', (msgC,rinfoC) =>{
   var lsls,rinfo;
   try {
       lsls = dp.decode(msgC);
       //console.log(lsls);
       if(!Ecache[lsls.questions[0].name]){
           for (const key in lsls.answers) {//遍历得到的ip
               if (lsls.answers.hasOwnProperty(key) && lsls.answers[key].type == 'A') {
                   lsls.answers[key].ttl = 5;//修改过期时间
               }
           } 
       }
       var name = lsls.questions[0].name+'_#_xhxx123_#_'+lsls.id;
       rinfo = sendList[name];
       if(!rinfo) {
           throw new Error(name);
       }
       delete sendList[name];
       msgC = dp.encode(lsls);
   } catch (error) {
       //console.log(error);
       //console.log(lsls);
       return;
   }
   udpServer.send(msgC, rinfo.port, rinfo.address, (err) => {});
});

//结束 解析114 事件处理





var sendList={};//查询列表
//处理53端口请求
udpServer.on('message', function(msg, rinfo){
   //console.log(this.address(),this);
   
   try {
       var data = dp.decode(msg);
       if(data.questions[0].type == 'A' && ztzt){//是不是解析 域名=>ip
           var host = data.questions[0].name;
           if(host == 'zhua.lingmax.top' && global.zhua){
               //定死抓zhua.LingMax.top 跳转成pac
               var fMsg = dp.encode(wzData('zhua.lingmax.top',global.zhua,data.id));
               udpServer.send(fMsg, rinfo.port, rinfo.address, (err) => {});
               return; //不走其他
           }else 
           if(typeof Dcache[host] == 'object'){//判断是不是有缓存
               
               Dcache[host].id = data.id;
               var fMsg = dp.encode(Dcache[host]);
               udpServer.send(fMsg, rinfo.port, rinfo.address, (err) => {});
               lastDcache[host].jc++;
               lastDcache[host].ip=Dcache[host].answers[0].data;
               //console.log('触发缓存'+data.questions[0].name);
               //console.log("cache",Dcache[data.questions[0].name])
               //console.log("data",data)
               return; //不走其他
           }else{
               //创建一个新缓存
               //console.log(data,'创建缓存');
               var sj = dp.streamEncode(data);//tcp专用数据包
               arrLd.push({'host':host,'msg':sj,'ddid':data.id});
               //走旧缓存 历史数据
               if(DNS_jhc && lastDcache[host]){//
                   var fMsg = dp.encode(wzData(host,lastDcache[host].ip,data.id,5));
                   udpServer.send(fMsg, rinfo.port, rinfo.address, (err) => {});
                   return; //不走其他
               }
           }

       }
       
       //其他解析走114解析
       sendList[data.questions[0].name+'_#_xhxx123_#_'+data.id] = {
           'port':rinfo.port,
           'address':rinfo.address,
           'time':new Date().getTime()+3000,//快速过期
       };
       udpC.send(msg, 53, '114.114.114.114', (err) => {});//114
       udpC.send(msg, 53, '119.29.29.29', (err) => {});//腾讯
           
   } catch (error) {}

});
udpServer.bind(53);

setInterval(() => {
   var dd = arrLd.pop();
   if(!dd) return;
   dnsCacheFun(dd.host,dd.msg,dd.ddid);
}, 150);//最多每秒处理n个 创建缓存 列队限速



function run_data(pack,address,host){//解析结果
    //console.log(arguments);

    var lsls = pack;
    var ee = address;
    var name = host+'_#_'+ee+'_#_'+lsls.id;
    var arr = sendList[name];
    if(!arr){ 
        return;
    }
    delete sendList[name];
    arr=arr.arr;
 
    for (var key1 in lsls.answers) {//遍历得到的ip
        if (lsls.answers.hasOwnProperty(key1) && lsls.answers[key1].type == 'A') {
            var ipv4 = lsls.answers[key1].data;
            if(arr[ipv4]|| '0.0.0.0'==ipv4||'127.0.0.1'==ipv4) continue;//无效IP
            arr[ipv4] = {
                'ip':ipv4,
                'host':host,
                'dns':ee,
                'data':lsls,
                'ping':9999,
                'tcp':9999,
                'zs':9999
            };
            arr[ipv4].data.answers = [{
                'class':"IN",
                'data':ipv4,
                'flush':false,
                'name':host,
                'ttl':295,//设置有效期295秒
                'type':"A"
            }];
            (function(){//创造变量空间
                var tt = new Date().getTime();
                //检测80端口
                var socket = new net.Socket()
                socket.setTimeout(2000);
                socket.on('connect', function() {
                    arr[ipv4].tcp = new Date().getTime() - tt;
                    socket.destroy();
                });
                socket.on('error', function() {socket.destroy();});
                socket.on('timeout', function() {socket.destroy();});
                socket.connect(80, ipv4);
                //socket.connect(443, ipv4);
 
                var ttp = new Date().getTime();
                //检测ping
                var session = ping.createSession({'sessionId': parseInt(Math.random()*(99999999-99999+1)+99999,10)});
                session.pingHost(ipv4,(err, target)=>{
                    if(err === null){
                        arr[ipv4].ping = new Date().getTime() - ttp;
                    }
                    session.close(); 
                });                  
            })();
        }
    }
 }



 var dnsServer = [
    '124.202.220.146',//华北->北京
    '202.96.69.38',//东北->辽宁
    //'124.133.43.90',//华东->山东青岛
    '210.21.223.204',//华南->广东
    //'118.182.148.250',//西北->甘肃
 //    '218.75.136.129',//华中->湖南
    '113.204.69.42',//西南->重庆
    //'124.117.230.210',//新疆
    '202.14.67.4',//香港
    '210.68.97.1',//台湾
    '205.171.3.65',//美国
    '203.2.193.67',//澳大利亚
    '210.220.163.82',//韩国
    '142.103.1.1',//加拿大
    '211.121.135.130',//日本
    '91.214.72.33',//意大利
    //'5.159.215.254',//英国
    //'185.32.20.62',//阿尔巴尼亚
    '195.214.240.136',//法国
    '212.66.160.2',//西班牙
    //'187.1.175.81',//巴西
    '91.217.62.219',//俄罗斯
    '213.188.101.15',//德国
    '202.129.240.138',//印度
    '222.255.121.132',//越南
    '202.136.162.11',//新加坡
    '193.67.79.39',//荷兰
 //    '119.29.29.29',//保底
     '208.67.222.222',//保底
 ];




var dnsServer_TCP={};

//刷新tcp连接状态
function fun_server() {
    for (const k in dnsServer) {
        if (dnsServer.hasOwnProperty(k) &&  !dnsServer_TCP[dnsServer[k]]) {
            (function () {
                var ipv4 = dnsServer[k];
                var socket = new net.Socket()
                socket.setTimeout(2000);//超时
                socket.setKeepAlive(true,4000);
                socket.on('error', function() {
                    delete dnsServer_TCP[ipv4];
                    // console.log("dns连接 error",ipv4,arguments);
                    
                    socket.destroy();
                });
                socket.on('timeout', function(){
                    delete dnsServer_TCP[ipv4];
                    //console.log("dns连接 timeout",ipv4,arguments);
                    socket.destroy();
                });
                socket.on('close', function(){
                    delete dnsServer_TCP[ipv4];
                    //console.log("dns连接 close",ipv4,arguments);
                    // socket.connect({
                    //     'port' :53,
                    //     'host': ipv4,
                    //     hints: dns.ADDRCONFIG ,
                    // },()=> {
                    //     dnsServer_TCP[ipv4] = socket;
                    // });
                    socket.destroy();
                });
                socket.on('data', (data)=>{
                    var host,lsls;
                    try {
                        lsls = dp.streamDecode(data);
                        host= lsls.questions[0].name;
                    } catch (error) {
                       // console.log("数据包解析错误",ipv4,error);
                        
                        return;
                    }
                    run_data(lsls,ipv4,host);
                });
                socket.connect({
                    'port' :53,
                    'host': ipv4,
                    hints: dns.ADDRCONFIG ,
                },()=> {
                    socket.setTimeout(8000);//超时
                    dnsServer_TCP[ipv4] = socket;
                    // console.log("连接 connect",ipv4,arguments);
                });
            })();

        }
        
    }
    
}
fun_server();

setInterval(fun_server, 2000);//每隔n秒连一次DNS服务器


var udpCx = dgram.createSocket('udp4');
udpCx.on('message',(msgC,rinfoC)=>{
    var arr,host,ee,lsls;
    try {
        lsls = dp.decode(msgC);
        host= lsls.questions[0].name;
    } catch (error) {
        return;
    }
    run_data(lsls,rinfoC.address,host);
});



// dnsCacheFun('www.taobao.com',function(c){
// });
/**
* @Function: 	获取指定域名的dns缓存
* @Author: 	LingMax[xiaohxx@qq.com]
* @Params: 	String	host		域名
* @Return: 	*
*/
function dnsCacheFun(host,dd,ddid){
   var arr = {};
   if(Ecache[host]) return;
   Ecache[host] = true;
   //1.5秒后开始 检测结果
   setTimeout(()=>{
       //单通道模式
       var ret = Object.keys(arr).map(function(keyxx) {return arr[keyxx];});
       for (const key22 in ret) {
           if (ret.hasOwnProperty(key22)) {
               const b = ret[key22];
               if(b.tcp==9999 && b.ping!=9999){
                   ret[key22].zs = b.ping*2;
               }else if(b.tcp!=9999 && b.ping==9999){
                   ret[key22].zs = b.tcp*2;
               }else{
                   ret[key22].zs = b.tcp+b.ping;
               }
           }
       }
       var retArr = ret.sort((a,b)=>{
           return a.zs-b.zs;
       });
       ret = retArr[0];
       
       if(!ret){
           //Ecache[host] = true;
           //bigCache(0,host,retArr);
           //console.log(retArr.length+" 放弃空值 "+ host);
           return;//放弃
       }else if(ret.zs == 19998){
           //Ecache[host] = true;
           //bigCache(1,host,retArr);
           //console.log(retArr.length + " 放弃超出 " + host + " => " + ret.zs);
           //console.log(retArr);
           return;//放弃
       }else{
           //console.log("IP:"+retArr.length+" TCP80:"+ret.tcp+" Ping:"+ret.ping+"   延迟:"+ret.zs+"  优化结果:"+ret.host+"    =>  "+ret.ip);
           ret.data.retArr = retArr;
           ret.data.host = host;
           ret.data.ltime = new Date().getTime()+delCacheTime;
           Dcache[host] = ret.data;
           if(!lastDcache[host]){
               lastDcache[host] = {
                   'jc':1,//计次
                   'ip':Dcache[host].answers[0].data,
               };
           }else{
               lastDcache[host].jc++;
               lastDcache[host].ip=Dcache[host].answers[0].data;
           }
           //fun(ret.data);
           //if(ret.zs > 700){
           //    bigCache(2,host,retArr);
           //}
       }
       return ;
   },2500);
   //结束检查结果
   var tti = new Date().getTime()+3000;
   //开始查询udp dns
   //var msgCC = dp.encode(dd);//生成查询数据包
   for (var key in dnsServer_TCP) {//循环dns 各地 服务器 每查到一个ip都添加进arr
       if (dnsServer_TCP.hasOwnProperty(key)) {
           (function(){//创造变量空间
               var ee = key;
               // var udpC = dgram.createSocket('udp4');
               // setTimeout(()=>{udpC.close();},2000);//定时销毁
               sendList[host+'_#_'+ee+'_#_'+ddid] = {
                   'arr':arr,
                   'time':tti,//快速过期
               };
               dnsServer_TCP[ee].write(dd);
               //udpCx.send(dd, 53, ee, (err) => {});
           })();
       }
   }
}

/**
* 伪造一个DNS Dcache数组配置
* @param {*} host  域名
* @param {*} gotoIP ip
* @param {*} id    dnsid
* @param {*} ttl  系统缓存过期秒数
* @param {*} gq    软件缓存过期秒数
*/
function wzData(host,gotoIP,id,ttl=60,gq=11352960000000) {
   var lsd = {"id":id,"type":"response","flags":384,"flag_qr":true,"opcode":"QUERY","flag_aa":false,"flag_tc":false,"flag_rd":true,"flag_ra":true,"flag_z":false,"flag_ad":false,"flag_cd":false,"rcode":"NOERROR","questions":[{"name":host,"type":"A","class":"IN"}],"answers":[{"class":"IN","data":gotoIP,"flush":false,"name":host,"ttl":ttl,"type":"A"}],"authorities":[],"additionals":[],"retArr":[{"ip":gotoIP,"host":host,"dns":"劫持配置","data":null,"ping":1,"tcp":1,"zs":1}],"host":host,"ltime":new Date().getTime()+gq};
   lsd.retArr[0].data=lsd;
   return lsd;
}



